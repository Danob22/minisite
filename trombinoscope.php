<?php
include_once "connexion.php";
$req4 ="SELECT * FROM user";
$res4 =  $pdo->query($req4);
$result4= $res4->fetchAll(PDO::FETCH_ASSOC);
?> 

<!doctype html>
<html lang = "fr">
<head>
	<meta charset = "utf-8">
	<title>Mini Site</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >	
</head>
	<body>
		<div class="container">
			<div class="row">
			  <?php foreach($result4 as $r){?>
			  <div class="col-sm-4">
				<div class="card text-center border-dark mb-3" style="width: 18rem;">
				  <a href="<?php echo $r['lien'] ?>"><img class="card-img-top" src="<?php echo "img/".$r['file']?>"></a>
				  <div class="card-body">
					<h5 class="card-title"><?php echo $r['nom'].' '.$r['prenom']?></h5>					
				  </div>
				</div>
			  </div>
			  <?php }?>		  
			</div>
			<a href="user.php" class="btn btn-primary" role="button">Retourner</a>	
		</div>
	</body>
</html>
