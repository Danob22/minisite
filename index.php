<!doctype html>
<?php    
session_start();    
if(isset($_SESSION['views'])) 
    $_SESSION['views'] = $_SESSION['views']+1; 
else
    $_SESSION['views']=1;
?>


<html lang = "fr">
<head>
	<meta charset = "utf-8">
	<title>Mini Site</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
	<style> 
		.container{
			margin : 0 auto;
			text-align: center;
			
		}
	</style>
</head>
	<body>		
		<div class="container">	
			<div class="d-flex justify-content-center h-100">
				<div class="card">
					<div class="card-header">
						<h2>Mini - Site</h2>
						<h2>Annuaire</h2>
					</div>
					<div class="card-body">
						<a href="admin.php" class="btn btn-primary" role="button">Administrateurs</a>
						<a href="user.php" class="btn btn-primary"> Utilisateurs</a>	
					</div>
					<div class="card-footer">
						<h4>IMT ATLANTIQUE</h4>
					</div>
				</div>
			</div>
		</div>	
	</body>
</html>
