<?php    
session_start();  
?>

<?php
include_once "connexion.php";
$req ="SELECT count(id) as masculin FROM user WHERE sexe = 'Masculin'";
$res =  $pdo->query($req);
$result= $res->fetchAll(PDO::FETCH_ASSOC);
$n_masculin = $result[0]['masculin'];

$req2 ="SELECT count(id) as total FROM user";
$res2 =  $pdo->query($req2);
$result2= $res2->fetchAll(PDO::FETCH_ASSOC);
$n_feminin = $result2[0]['total'] - $n_masculin;

$pour_masculin = (($n_masculin/$result2[0]['total'])*100);
$pour_feminin = (($n_feminin/$result2[0]['total'])*100);
?> 

<!doctype html>
<html lang = "fr">
<head>
	<meta charset = "utf-8">
	<title>Mini Site</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >	
</head>
	<body>
		<div class="container">										
			<table class="table table-bordered">
				<thead>
				  <tr>
					<th>Sexe</th>
					<th>Nombre de personnes</th>
					<th>Pourcentage</th>
				  </tr>
				</thead>
				<tbody>
				  <tr>
					<td>Masculin</td>
					<td><?php echo $n_masculin?> </td>
					<td><?php echo $pour_masculin ?> &#37 </td>
				  </tr>
				  <tr>
					<td>Feminin</td>
					<td><?php echo $n_feminin?> </td>
					<td><?php echo $pour_feminin ?> &#37 </td>
				  </tr>
				</tbody>
			</table>
			
			<h5><?php echo "Nombre de visites = ".$_SESSION['views']; ?></h5>	
			<a href="admin.php" class="btn btn-primary" role="button">Retourner</a>
	
		</div>
	</body>
</html>

