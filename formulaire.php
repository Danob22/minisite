<?php
include_once "connexion.php";

if(isset($_GET['id'])){
	$req="SELECT * FROM `user` WHERE id = ".$_GET['id']; 		
	$res =  $pdo->query($req);
	$result= $res->fetchAll(PDO::FETCH_ASSOC);
	$sexe = $result[0]['sexe'];
}
?> 

<!doctype html>
<html lang = "fr">
<head>
	<meta charset = "utf-8">
	<title>Mini Site</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){
			console.log('<?php echo $sexe ?>' == 'Feminin');
			if('<?php echo $sexe ?>' == 'Masculin'){
				console.log('Masculin');
				$('#Sexe option:eq(0)').prop('selected',true);				
			}else if('<?php echo $sexe ?>' == 'Feminin'){
				console.log('Feminin');
				$('#Sexe option:eq(1)').prop('selected',true);		
			}
		});			
	</script>		
</head>
	<body>
		<div class="container">
			<form method="post" action="add.php" enctype="multipart/form-data">
				<div class="form-group">
					<label for = "Nom">Nom: *</label>
					<input type = "text" name = "nom" id = "Nom" maxlength="30" class="form-control"  required value = "<?php  echo isset($result[0]['nom']) ? $result[0]['nom']: null; ?>">
				</div>
				<div class="form-group">
					<label for = "Prenom">Prenom: *</label>
					 <input type = "text" name = "prenom" id = "Prenom" maxlength="15" class="form-control" required value="<?php echo isset($result[0]['prenom']) ? $result[0]['prenom']: null; ?>">
				</div>
				<div class="form-group">
					<label for = "Email">Email: *</label>
					<input type = "email" name = "email" id = "Email" class="form-control" required value="<?php echo isset($result[0]['email']) ? $result[0]['email']: null; ?>">
				</div>
				<div class="form-group">
					<label for = "Age">Age: *</label> 
					<input type = "number" name = "age" id = "Age" min="18" max="99" step="1"  pattern="\d+" class="form-control" required value="<?php echo isset($result[0]['age']) ? $result[0]['age']: 18; ?>">
				</div>
				<div class="form-group">
					<label for = "Sexe">Sexe: </label> 
					<select class="form-control" name = "sexe" id = "Sexe" required>
					  <option value="Masculin" id="Masculin">M</option>
					  <option value="Feminin" id="Feminin">F</option>					  
					</select>
				</div>
				<div class="form-group">
					<label for = "fileToUpload">Image: </label>
					<input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
					<input type="text" name="file" id="file" class="form-control" hidden value="<?php echo isset($result[0]['file']) ? $result[0]['file']: null; ?>">
				</div>
				
				<div class="form-group">
					<label for = "Lien">Link à la page personnelle: *</label>
					<input type = "url" name = "lien" id = "Lien" class="form-control" placeholder="https://example.com" pattern="https://.*" required value="<?php echo isset($result[0]['lien']) ? $result[0]['lien']: null; ?>">
				</div>
				<?php if(isset($_GET['id'])){ ?>									
					<input type = "submit" value = "Valider" name = "submit" class="btn btn-primary" formaction="modifier.php?id= <?php echo $_GET['id']?>"">
				<?php }else{ ?>
					<input type = "submit" value = "Valider" name = "submit" class="btn btn-primary">
				<?php }?>
				<a href="admin.php" class="btn btn-info" role="button">Retourner</a>
			</form>			
		</div>
	</body>
</html>
