<?php
include_once "connexion.php";
$req4 ="SELECT * FROM user";
$res4 =  $pdo->query($req4);
$result4= $res4->fetchAll(PDO::FETCH_ASSOC);
?> 

<?php
function pluralize( $count, $text )
{
    return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
}

function ago( $datetime )
{
    $interval = date_create('now')->diff( $datetime );
    $suffix = ( $interval->invert ? ' ago' : '' );
    if ( $v = $interval->y >= 1 ) return pluralize( $interval->y, 'year' ) . $suffix;
    if ( $v = $interval->m >= 1 ) return pluralize( $interval->m, 'month' ) . $suffix;
    if ( $v = $interval->d >= 1 ) return pluralize( $interval->d, 'day' ) . $suffix;
    if ( $v = $interval->h >= 1 ) return pluralize( $interval->h, 'hour' ) . $suffix;
    if ( $v = $interval->i >= 1 ) return pluralize( $interval->i, 'minute' ) . $suffix;
    return pluralize( $interval->s, 'second' ) . $suffix;
}
?>
<!doctype html>
<html lang = "fr">
<head>
	<meta charset = "utf-8">
	<title>Mini Site</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >	
	<style> 
		.container{
			margin : 0 auto;
			text-align: center;
		}
	</style>
</head>
	<body>	
		<div class="container">		
			<a href="formulaire.php" class="btn btn-primary" role="button">Ajorter une personne</a>
			<a href="statistiques.php" class="btn btn-primary" role="button">statistiques</a>		
			<table class="table table-bordered">
				<thead>
				  <tr>
					<th>Id</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>Sexe</th>
					<th>Age</th>
					<th>Duree</th>
					<th>Photo</th>
					<th>Modifier</th>
					<th>Supprimer</th>
				  </tr>
				</thead>
				<tbody>
				  <?php foreach($result4 as $r){?>
				  <tr>
					<td><?php echo $r['id'] ?> </td>
					<td><?php echo $r['nom'] ?></td>
					<td><?php echo $r['prenom']?> </td>
					<td><?php echo $r['sexe']?> </td>
					<td><?php echo $r['age']?> </td>
					<td>
						<?php
						echo ago( new DateTime($r['inscription']) );
						?>
					</td>
					<td><img src = "<?php echo "img/".$r['file']?>" width = "20%" ></td>
					<td><a href="formulaire.php?id= <?php echo $r['id']?>"><span class="glyphicon glyphicon-edit">M</span></a></td>
					<td><a href="supprimer.php?id= <?php echo $r['id']?>"><span class="glyphicon glyphicon-trash">S</span></a></td>	
				  </tr>		
				  <?php }?>
				</tbody>
			</table>					
			<a href="index.php" class="btn btn-primary" role="button">Retourner</a>	
		</div>
	</body>
</html>




